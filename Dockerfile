FROM maven:3.6-jdk-11-slim
COPY . /src
WORKDIR /src
RUN mvn install -DskipTests

EXPOSE 8080
ENTRYPOINT ["java","-jar","/src/target/hello-0.0.1-SNAPSHOT.jar"]